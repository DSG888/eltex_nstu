/*
ЗАДАНИЕ:
В лабораторной работе требуется написать две программы для обработки текстовых файлов. Одна из них выполняет построчную, другая посимвольную обработку:
	2. Написать программу, выполняющую посимвольную обработку текстового файла (табл. 4).

Ввод параметров должен быть организован в командной строке запуска программы.
Исходный файл должен быть создан с помощью любого текстового редактора.
При обработке текста рекомендуется использовать функции из стандартной библиотеки СИ для работы со строками, преобразования и анализа символов.

Таблица 4.
5
Заменить каждый пробел на два 
Параметры командной строки:
	1. Имя входного файла 
	2. Количество замен 
*/

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LEN 1024

int main(int argc, char **argv){
	if (argc != 3){
		perror("NoParam\n");
		exit(1);
	}
	FILE* myfile = fopen(argv[1], "rt");
	FILE* myoutfile = fopen("out", "wt");
	if (!myfile){
		perror("NoFile\n");
		exit(EEXIST);
	}
	int lim = atoi(argv[2]);
	char buf[MAX_LEN] = {'\0'};
	register int c = '\0';
	register int i = 0, counter = 0; 
	while (((c = getc(myfile)) != EOF) && i < MAX_LEN){
		if(c == ' '){
			++counter;
		}
		else{
			if (c == '\n'){
				if (counter <= lim){
					buf[i] = '\0';
					//fprintf(myoutfile, "%s\n", buf);//FIXME
					for (int j = 0; j < i; ++j){
						fprintf(myoutfile, "%c", buf[i]);
					}
					fprintf(myoutfile, "\n");
					printf("[%d] %s\n", counter, buf);
				}
				else
					printf("[%d] \n", counter);
				counter = 0;
				i = 0;
				continue;
			}
		}
		buf[i++] = c;
	}
	fclose(myfile);
	fclose(myoutfile);
}
