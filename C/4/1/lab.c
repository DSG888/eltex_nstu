/*
ЗАДАНИЕ:
В лабораторной работе требуется написать две программы для обработки текстовых файлов. Одна из них выполняет построчную, другая посимвольную обработку:
	1. Написать программу, обрабатывающую текстовый файл и записывающую обработанные данные в файл с таким же именем, но с другим типом (табл. 3).

Ввод параметров должен быть организован в командной строке запуска программы.
Исходный файл должен быть создан с помощью любого текстового редактора.
При обработке текста рекомендуется использовать функции из стандартной библиотеки СИ для работы со строками, преобразования и анализа символов.

Таблица 3.
5
Исключить строки с количеством пробелов, большим заданного числа 
Параметры командной строки:
	1. Имя входного файла 
	2. Заданное количество пробелов 

*/

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024

int count(char* array, int ch, int n){
	int count = 0;
	for (int i = 0; (i < n && array[i] != '\0'); ++i)
		if (array[i] == ch)
			++count;
	return count;
}

int main(int argc, char **argv){
	if (argc != 3){
		perror("NoParam\n");
		exit(1);
	}
	FILE* myfile = fopen(argv[1], "rt");
	FILE* myoutfile = fopen("out", "wt");
	if (!myfile){
		perror("NoFile\n");
		exit(EEXIST);
	}
	int lim = atoi(argv[2]);
	char buf[MAX_LEN] = {'\0'};
	while (fgets(buf, MAX_LEN, myfile)){
		int co = count(buf, ' ', MAX_LEN);
		printf("[%d] ", co, buf);
		if (co <= lim){
			fprintf(myoutfile, "%s", buf);
			if (buf[strnlen(buf, MAX_LEN) - 1] == '\n')	// Костыль
				buf[strnlen(buf, MAX_LEN) - 1] = '\0';
			printf("%s\n", buf);
		}
		else
			printf("\n");
	}
	fclose(myfile);
	fclose(myoutfile);
	return 0;
}
