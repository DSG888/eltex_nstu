#include "db.h"

mystruct_t* readnode(){
	mystruct_t* newnode = malloc(sizeof(mystruct_t));
	char* name = malloc(sizeof(char) * MAX_LEN);
	if (!newnode || !name){
		perror("Memory error");
		exit(0);
	}
	printf("Новый элемент списка\n> Название: ");
	getchar();
	fgets(name, MAX_LEN, stdin);
	if (name[strnlen(name, MAX_LEN) - 1] == '\n')	// Костыль
		name[strnlen(name, MAX_LEN) - 1] = '\0';
	newnode->name = name;
	printf("> Дата покупки:\n>> Год покупки: ");
	scanf("%d", (unsigned int*)&newnode->year);
	printf(">> Месяц покупки: ");
	scanf("%d", (unsigned int*)&newnode->month);
	printf(">> День покупки: ");
	scanf("%d", (unsigned int*)&newnode->day);
	printf("> Стоимость: ");
	scanf("%d", (unsigned int*)&newnode->coast);
	printf("> Количество: ");
	scanf("%d", (unsigned int*)&newnode->count);
	return newnode;
}

void reestablish(mystruct_t** mas, uint16_t count){
/*	void QuickSort(mystruct_t** array[], uint16_t left, uint16_t right){
		int pivot = 6;//, tmp;
		uint16_t i = left, j = right;
		do {
			while (array[i]->month < pivot)
				i++;
			while (array[j]->month > pivot)
				j--;
			if (i <= j ){
		//		tmp = array[i];		//FIXME
		//		array[i] = array[j];
		//		array[j] = tmp;
				i++;
				j--;
			}
		} while (i < j);

		if (left < j)
			QuickSort(array, left, j);
		if (i < right)
			QuickSort(array, i, right);
	}*/
	for (uint16_t i = 1; i < count; ++i)
		for (uint16_t j = 0; j < count - 1; ++j)
			if(mas[j]->month > mas[j + 1]->month){
				mystruct_t* ctemp = mas[j];
				mas[j] = mas[j + 1];
				mas[j + 1] = ctemp;
			}
}

void printnode(mystruct_t* node){
//	printf("Покупка: %d * [%s]\nДата: [%d:%d:%d]\nСтоимость: [%d]\n", node->count, node->name, node->year, node->month, node->day, node->coast);
	printf("%s\t[%d:%d:%d]\t[%d]\tx%d\n", node->name, node->year, node->month, node->day, node->coast, node->count);
}

void printmas(mystruct_t** mas, uint16_t count){
	for (uint16_t i = 0; i < count; ++i){
		printnode(mas[i]);
	}
	printf("------------------");
}

void freemas(mystruct_t** mas, uint16_t count){
	for (uint16_t i = 0; i < count; ++i){
		free(mas[i]->name);
		free(mas[i]);
	}
	free(mas);
}

