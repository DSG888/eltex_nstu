#include <stdio.h>
#include <stdint.h>

#include "db.h"

#define DEBUG 0
/*
ЗАДАНИЕ:
	1. 
	Написать программу, работающую с базой данных в виде массива структур и выполняющую последовательный ввод данных в массив и последующую распечатку его содержимого. 
	Состав структуры приведен в варианте.
	Типы данных выбрать самостоятельно.
	При написании программы следует использовать статические массивы структур или указателей на структуру.
	Размерности массивов – 3–4. 
	Ввод данных выполнить с помощью функций scanf().

	2. 
	Модифицировать программу, используя массив указателей на структуру и динамическое выделение памяти. 
	Выполнить сортировку массива с помощью функции qsort. 
	Способ сортировки массива приведен в варианте.
	Для динамического выделения памяти используйте функцию malloc().
	Для определения размера структуры в байтах удобно использовать операцию sizeof(), возвращающую целую константу: 
		struct ELEM *sp; 
		sp = malloc(sizeof(struct ELEM));

	5
	Название покупки 
	Дата приобретения
	Стоимость 
	Количество 
	Сгруппировать все записи по месяцам приобретения 

*/

void printmenu(){
	printf("\n\n[1] Добавить покупку\n");
	printf("[2] Просмотрить массив\n");
	printf("[0] Выход\n\n> ");
}

int main(int argc, char **argv){
	mystruct_t** mas = malloc(sizeof(mystruct_t*) * MAX_LEN);
	if (!mas){
		perror("Memory error");
		exit(0);
	}
	uint16_t globalcounter= 0;
	#if DEBUG 
	printf("DEBUG\n");
	mystruct_t n0 = {"abc", 1999, 12, 1, 5000, 1};	mas[0] = &n0;
	mystruct_t n1 = {"zxc", 1999, 10, 1, 5000, 2};	mas[1] = &n1;
	mystruct_t n2 = {"asd", 1999, 8, 1, 5000, 3};	mas[2] = &n2;
	mystruct_t n3 = {"qwe", 1999, 6, 1, 5000, 4};	mas[3] = &n3;
	mystruct_t n4 = {"dfg", 1999, 4, 1, 5000, 5};	mas[4] = &n4;
	globalcounter = 5;
	printf("До сортировки:\n");
	printmas(mas, globalcounter);
	reestablish(mas, globalcounter);
	printf("После сортировки:\n");
	printmas(mas, globalcounter);
	#endif
	int Q = -1;
	do
	{
		printmenu();
		scanf("%d", &Q);
		switch(Q){
			case 0:
				break;
			case 1:
				if (globalcounter >= MAX_LEN){
					printf("Хватит\n");
					break;
				}
				mas[globalcounter++] = readnode();
				reestablish(mas, globalcounter);
				break;
			case 2:
				printmas(mas, globalcounter);
				break;
			default:
				printf("Такого пункта нет\n");
				break;
		}
	} while (Q);
	#if DEBUG == 0
	freemas(mas, globalcounter);
	#endif
	return 0;
}
