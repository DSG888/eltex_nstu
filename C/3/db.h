#ifndef DB_H
#define DB_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024

struct mystruct_s{
	char* name;					// Название покупки 
	uint16_t year, month, day;	// Дата приобретения
	uint16_t coast;				// Стоимость
	uint16_t count;				// Количество
} typedef mystruct_t;

mystruct_t* readnode();
void reestablish(mystruct_t** mas, uint16_t count);
void printmas(mystruct_t** mas, uint16_t count);
void printnode(mystruct_t* node);
void freemas(mystruct_t** mas, uint16_t count);


#endif
