#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define MAX_LEN 1024

/*
ЗАДАНИЕ:
	Написать программу сортировки массива строк по вариантам.
	Строки вводить с клавиатуры. Сначала пользователь вводит кол-во строк потом сами строки. 
	Для тестирования использовать перенаправление ввода-вывода ($./myprog < test.txt). 
	Память для строк выделять динамически с помощью функций malloc или calloc.
	Ввод данных, сортировку и вывод результатов оформить в виде функций.
	Сортировку делать с помощью qsort если возможно, если нет то писать свой вариант сортировки.
	Входные и выходные параметры функции сортировки указаны в варианте. 
	Входные и выходные параметры функций для ввода-вывода:

	Прототип функции для ввода одной строки 
	length = inp_str(char* string, int maxlen); 
	// length – длина строки 
	// string – введенная строка 
	// maxlen – максимально возможная длина строки (размерность массива string) 

	Прототип функции для вывода одной строки.
	void out_str(char* string, int length, int number);
	// string – выводимая строка 
	// length – длина строки 
	// number – номер строки 

	Модифицировать программу, применив в функциях передачу параметров и возврат результатов по ссылке (с использованием указателей).
	Сравнить результаты.

5
	Расположить строки по возрастанию количества слов 
	Входные параметры:
	1. Массив 
	2. Размерность массива 
	Выходные параметры:
	1. Количество перестановок 
	2. Первый символ последней строки 
*/

char** readMas(int count){
	char buffer[MAX_LEN] = {'\0'};
	char **mas = (char **)malloc(sizeof(char *)*count);// выделяем память для массива указателей
	if (!mas){
		memerror:
		perror("Memory error\n");
		exit(1);
	}
	for (int i = 0; i < count ; ++i){
		//scanf("%s\n", buffer); // читаем строку в буфер
		fgets(buffer, MAX_LEN, stdin);
		mas[i] = (char *)malloc(sizeof(char)*strnlen(buffer, MAX_LEN)); //выделяем память для строки
		if (!mas[i])
			goto memerror;
		if (buffer[strnlen(buffer, MAX_LEN) - 1] == '\n')	// Костыль
			buffer[strnlen(buffer, MAX_LEN) - 1] = '\0';
		strncpy(mas[i], buffer, strnlen(buffer, MAX_LEN) + 1); //копируем строку из буфера в массив указателей
	}
	return mas;
}

void printMas(char **mas, int count){
	for (int i = 0; i < count ; i++){
		printf("%s\n", mas[i]);
	}
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
		free(mas[i]); // освобождаем память для отдельной строки
	}
	free(mas); // освобождаем памать для массива указателей на строки
}

/*
	Функция сортировки строк по числу слов
		Сложность О(n^2)
*/
void sort(char **mas, int const count){
	uint16_t* masc = malloc(sizeof(uint16_t) * count);
	if (!masc){
		perror("Memory error\n");
		exit(1);
	}
	for (uint16_t i = 0; i < count; ++i){	// Получение числа слов в каждой строке
		for (uint16_t j = 0, flag = 0; j < MAX_LEN && mas[i][j] != '\0'; ++j)	// Обходим каждую строку
			if (!flag && mas[i][j] != ' '){
				++masc[i];
				flag = 1;
			}
			else
				if (flag && mas[i][j] == ' ')
					flag = 0;
	}
	uint16_t co = 0;
	for (uint16_t i = 1; i < count; ++i)
		for (uint16_t j = 0; j < count - 1; ++j)
			if(masc[j] > masc[j + 1]){
				++co;
				// Переставляем строки
				char* ctemp = mas[j];
				mas[j] = mas[j + 1];
				mas[j + 1] = ctemp;
				// Переставляем счетчики
				uint16_t temp = masc[j];
				masc[j] = masc[j + 1];
				masc[j + 1] = temp;
			}
	printf("Перестановок: %d\n", co);
	free(masc);
}

int main(int argc, char **argv){
	uint16_t count = 0;
	scanf("%d", &count);
	getchar();
	printf("\n");
	char **mas = readMas(count);
	sort(mas, count);
	printMas(mas, count);
	freeMas(mas, count);
	return 0;
}
