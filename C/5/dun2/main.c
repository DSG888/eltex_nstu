/*
ЗАДАНИЕ:
1.Разработать функции для выполнения арифметических операций по вариантам. 
2.Оформить статическую библиотеку функций и написать программу, ее использующую.
3.Переоформить библиотеку, как динамическую, но подгружать статически, при компиляции.
4.Изменить программу для динамической загрузки функций из библиотеки.

Варианты заданий. 
5.Операции возведения в куб и четвертую степень.
*/

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
//#include "libao.h"

void *library_handler;

int main(int argc, char **argv){
	//void *dlopen (const char *filename, int flag);
	library_handler = dlopen("./libao.so", RTLD_LAZY);
	if (!library_handler){
		//если ошибка, то вывести ее на экран
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		exit(1); // в случае ошибки можно, например, закончить работу программы
	};
//void (* test)() = dlsym( handle, "myfunc");
//(* test)();

	//void *dlsym(void *handle, char *symbol);
	int (* s3)(int i) = dlsym(library_handler ,"s3");
	int (* s4)(int i) = dlsym(library_handler ,"s4");
	
	//dlopen("./libao.so", );
	printf("%d\n", s3(2));
	printf("%d\n", s4(2));
	dlclose(library_handler);
}
