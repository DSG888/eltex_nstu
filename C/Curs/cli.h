#ifndef CLI_H
#define CLI_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "main.h"

#define MAX_MSG 256
#define fmt_str_len 23
#define date_format "[%Y.%m.%d|%H:%M:%S]"
#define number_of_milliseconds_in_second 1000	// Число миллисекунд в секунде
#define number_of_microseconds_in_second number_of_milliseconds_in_second * 1000	// Число микросекунд в секунде .. Нужно если заказчик управляет временем
#define MIN_SLEEP 1
#define MAX_SLEEP 3

unsigned int rand_int(unsigned int min, unsigned int max);
unsigned long wait_server_msg(int port, char* msg);
int client_connect(unsigned int ip, int port);
void* client1_handler(void *Args);
char* gen_rnd_msg(int size);
void* client1_handler(void *Args);
void* client2_handler(void *Args);

#endif
