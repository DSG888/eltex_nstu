#include "cli.h"

inline unsigned int rand_int(unsigned int min, unsigned int max) {
	return (rand() % (max - min + 1)) + min;
}

// Отлов сообщения
unsigned long wait_server_msg(int port, char* msg) {
	// set timeout to 2 seconds.
	struct timeval timeV = {2, 0};	// TODO Возможно стоит сделать это число изменяемым в зависимости от числа клиентов на локальной машине
	int enabledflag = 1;
	while (work) {	// Цикл на открытие соккета
		if (DEBUG)
			fprintf(stderr, "Начал слушать\n");
		int sock_n;
		struct sockaddr_in cliAddr, servAddr;
		char buf[MAX_MSG];
		sock_n = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if(sock_n < 0) {
			fprintf(stderr, "cannot open socket\n");
			exit(EXIT_FAILURE);
		}
		if (setsockopt(sock_n, SOL_SOCKET, SO_BROADCAST, &enabledflag, sizeof(enabledflag)) == -1) {
			fprintf(stderr, "setsockopt (SO_BROADCAST)\n");
			exit(EXIT_FAILURE);
		}
		// BROADCAST который мы заслужили
		if (setsockopt(sock_n, SOL_SOCKET, SO_REUSEADDR, &enabledflag, sizeof(enabledflag)) == -1) {
			fprintf(stderr, "setsockopt (SO_REUSEADDR)\n");
			exit(EXIT_FAILURE);
		}
		if (setsockopt(sock_n, SOL_SOCKET, SO_RCVTIMEO, &timeV, sizeof(timeV)) == -1) {
			fprintf(stderr, "setsockopt (SO_RCVTIMEO)\n");
			exit(EXIT_FAILURE);
		}
		servAddr.sin_family = AF_INET;
		servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		servAddr.sin_port = htons(SERVER_UDP_PORT);
		if (0 > bind(sock_n, (struct sockaddr *) &servAddr,sizeof(servAddr))) {
			fprintf(stderr, "cannot bind port number %d\n", SERVER_UDP_PORT);
			exit(EXIT_FAILURE);
		}
		while(work) { // Цикл на случай получения мусора на порт
			memset(buf, 0, MAX_MSG); // TODO check
			// receive message 
			int cliLen = sizeof(cliAddr);
			int n = recvfrom(sock_n, buf, MAX_MSG, 0, (struct sockaddr *) &cliAddr, (socklen_t *)&cliLen);
			if (n < 0) {
				if (DEBUG)
					fprintf(stderr, "Широковещательный пакет не получен\n");
				break;	// Время вышло. Освобождаем соккет
			}
			if (!strncmp(msg, buf, MAX_MSG)) {
				close(sock_n);
				if (DEBUG) {
					fprintf(stderr, "Получено сообщение %s от %s\n", msg, inet_ntoa(cliAddr.sin_addr));
					fflush(stdout);
				}
				return cliAddr.sin_addr.s_addr;
			}
		}
		close(sock_n);
		//TODO Добавить рендомную паузу
	}
	// Недостижимый код
	return 0;
}

int client_connect(unsigned int ip, int port) {
	int sock;
	struct sockaddr_in client;
	sock = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP);
	if (sock == -1) {
		fprintf(stderr, "Could not create socket\n");
		exit(EXIT_FAILURE);
	}
	memset(&client, 0, sizeof(client));
	client.sin_addr.s_addr = ip;
	client.sin_family = AF_INET;
	client.sin_port = htons(port);
	if (connect(sock, (struct sockaddr *)&client , sizeof(client)) < 0) {
		return -1;
	}
	return sock;
}

char* gen_rnd_msg(int size) {
	static int counter = 0;
	if (fmt_str_len > size)	// [2018.08.01|15:24:07] @\0
		return 0;
	char* str = malloc(sizeof(char) * (size + 1));
	if (!str)
		return 0;
	struct timeval tv;
	struct timezone tz;
	struct tm tm;
	gettimeofday(&tv, &tz);//tz?
	localtime_r(&tv.tv_sec, &tm);
	strftime(str, fmt_str_len - 1, date_format, &tm);
	str[fmt_str_len - 2] = ' ';
	for (int i = fmt_str_len - 1; i < size; ++i) {
		if (rand() % 2)
			str[i] = rand_int('a', 'z');
		else
			str[i] = rand_int('A', 'Z');
	}
	if (DEBUG) {
		char buf[MAX_MSG] = {'\0'};
		snprintf(buf, MAX_MSG, "$%d$%d", counter++, size);
		strcpy(str + size - strnlen(buf, MAX_MSG), buf);
	}
	str[size] = '\0';
	return str;
}

void* client1_handler(void *Args) {
	pthread_detach(pthread_self());		// Guarantees that thread resources are deallocated upon return
	
	int slen = rand_int(DEBUG?fmt_str_len:fmt_str_len + 5, DEBUG?69:MAX_MSG);		//FIXME 69
	char* str = gen_rnd_msg(slen);
	if (!str)
		DieWithError("Memerror");
	while (work) {
		// Получение IP сервера
		struct in_addr srv_ip = {wait_server_msg(SERVER_UDP_PORT, REQ_MSG)};
		if (!work)
			break;
		fprintf(stdout, "Подключение %s%s%s:%s%d%s>", cl_blue, inet_ntoa(srv_ip), cl_normal, cl_blue, CLIENT1_TCP_PORT, cl_normal);
		int sock = client_connect(*((unsigned int*)Args), CLIENT1_TCP_PORT);
		if (0 > sock) {
			fprintf(stdout, "[%sFAIL%s]\n", cl_red, cl_normal);
			continue;
		}
		else
			fprintf(stdout, "[%sОК%s]\n", cl_green, cl_normal);
		while (work) {
			fprintf(stdout, "> %s ", str);
			int err = 0, sleep_time = rand_int(MIN_SLEEP * number_of_milliseconds_in_second, MAX_SLEEP * number_of_milliseconds_in_second);
			if ((err = send_msg(sock, str, slen)) != 0) {
				fprintf(stdout, "[%sFAIL%s]\n", cl_red, cl_normal);
				if (err == 4) {
					usleep(sleep_time*1000);
					continue;
				}
				close(sock);
				break;
			}
			fprintf(stdout, "[%sОК%s] %s%dms%s\n", cl_green, cl_normal, cl_blue, sleep_time, cl_normal);
			free(str);
			// Генерация новой строки
			slen = rand_int(DEBUG?fmt_str_len:fmt_str_len+5, DEBUG?69:MAX_MSG);		//FIXME 69
			str = gen_rnd_msg(slen);
			if (!str)
				DieWithError("Memerror");
			usleep(sleep_time*1000);
		}
	}
	return NULL;
}

void* client2_handler(void *Args) {
	pthread_detach(pthread_self());		// Guarantees that thread resources are deallocated upon return
	
	while (work) {
		struct in_addr srv_ip = {wait_server_msg(SERVER_UDP_PORT, DIS_MSG)};
		fprintf(stderr, "Подключаюсь к %s:%d>\n", inet_ntoa(srv_ip), CLIENT1_TCP_PORT);
		if (!work)
			break;
		fprintf(stdout, "Подключение %s%s%s:%s%d%s>", cl_blue, inet_ntoa(srv_ip), cl_normal, cl_blue, CLIENT1_TCP_PORT, cl_normal);
		int sock = client_connect(*((unsigned int*)Args), CLIENT2_TCP_PORT);
		if (0 > sock) {
			fprintf(stdout, "[%sFAIL%s]\n", cl_red, cl_normal);
			continue;
		}
		else
			fprintf(stdout, "[%sОК%s]\n", cl_green, cl_normal);
		while (work) {
			int sleep_time = rand_int(MIN_SLEEP * number_of_milliseconds_in_second, MAX_SLEEP * number_of_milliseconds_in_second);
			char* str = NULL;
			if (!(str = recv_msg(sock, 0))) {
				fprintf(stdout, "[%sFAIL%s]\nЖду широковещательный пакет от сервера\n", cl_red, cl_normal);
				close(sock);
				break;
			}
			fprintf(stdout, "[%sОК%s] %s %s%dms%s\n", cl_green, cl_normal, str, cl_blue, sleep_time, cl_normal);
			free(str);
			usleep(sleep_time * 1000);
		}
		sleep(1);
	}
	return NULL;
}

