#define CTEST_MAIN
#include "ctest/ctest.h"

#include "../linkedlist.h"
#include "../queue.h"

CTEST(LinkedList, create_free) {
	ll_t* msglist = ll_create();
	ASSERT_NOT_NULL(msglist);
	ll_free(msglist);
}

CTEST(LinkedList, add_head) {
	ll_t* msglist = ll_create();
	ASSERT_NOT_NULL(msglist);
	ASSERT_FALSE(ll_add_head(msglist, "1"));
	ASSERT_FALSE(ll_add_head(msglist, "2"));
	ASSERT_FALSE(ll_add_head(msglist, "3"));
	ll_free(msglist);
}

CTEST(LinkedList, add_tail) {
	ll_t* msglist = ll_create();
	ASSERT_NOT_NULL(msglist);
	ASSERT_FALSE(ll_add_tail(msglist, "1"));
	ASSERT_FALSE(ll_add_tail(msglist, "2"));
	ASSERT_FALSE(ll_add_tail(msglist, "3"));
	ll_free(msglist);
}

CTEST(LinkedList, del_head) {
	ll_t* msglist = ll_create();
	ASSERT_NOT_NULL(msglist);
	ll_add_tail(msglist, "1");
	ll_add_tail(msglist, "2");
	ll_add_tail(msglist, "3");
	ASSERT_FALSE(ll_del_head(msglist));	
	ASSERT_FALSE(ll_del_head(msglist));
	ASSERT_FALSE(ll_del_head(msglist));
	ASSERT_FALSE(msglist->count);
	ASSERT_TRUE(ll_del_head(msglist));
	ll_free(msglist);
}

CTEST(LinkedList, del_tail) {
	ll_t* msglist = ll_create();
	ASSERT_NOT_NULL(msglist);
	ll_add_tail(msglist, "1");
	ll_add_tail(msglist, "2");
	ll_add_tail(msglist, "3");
	ASSERT_FALSE(ll_del_tail(msglist));	
	ASSERT_FALSE(ll_del_tail(msglist));
	ASSERT_FALSE(ll_del_tail(msglist));
	ASSERT_FALSE(msglist->count);
	ASSERT_TRUE(ll_del_tail(msglist));
	ll_free(msglist);
}

CTEST(Queue, create_free) {
	q_t* msgqueue = q_create(10);
	ASSERT_NOT_NULL(msgqueue);
	q_free(msgqueue);
}

CTEST(Queue, add) {
	q_t* msgqueue = q_create(3);
	ASSERT_FALSE(q_push(msgqueue, "1"));
	ASSERT_FALSE(q_push(msgqueue, "2"));
	ASSERT_FALSE(q_push(msgqueue, "3"));
	ASSERT_TRUE(q_push(msgqueue, "4"));
	//ll_print(msgqueue->list);
	q_free(msgqueue);
}

CTEST(Queue, del) {
	q_t* msgqueue = q_create(2);
	char* str1[] = {"1"}, str2[] = {"2"};
	char* str1_r = NULL;
	char* str2_r = NULL;;
	q_push(msgqueue, str1);
	q_push(msgqueue, str2);
	ASSERT_NOT_NULL(str2_r = q_pop(msgqueue));
	ASSERT_NOT_NULL(str1_r = q_pop(msgqueue));
	ASSERT_EQUAL_U((long unsigned int)str1, (long unsigned int)str1_r);
	ASSERT_EQUAL_U((long unsigned int)str2, (long unsigned int)str2_r);
	q_free(msgqueue);
}

int main(int argc, const char** argv) {
	return ctest_main(argc, argv);
}
