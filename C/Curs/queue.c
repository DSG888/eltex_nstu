#include "queue.h"

q_t* q_create(int queue_length) {
	q_t* queue = malloc(sizeof(q_t));
	if (!queue && queue_length >= 0)
		return NULL;
	queue->list = ll_create();
	if (!queue->list)
		return NULL;
	queue->queue_length = queue_length;
	pthread_mutex_init(&(queue->qmutex), NULL);
	return queue;
}

void q_free(q_t* queue) {
	pthread_mutex_lock(&(queue->qmutex));
	if (!queue)
		return;
	ll_free(queue->list);
	pthread_mutex_unlock(&(queue->qmutex));
	pthread_mutex_destroy(&(queue->qmutex));
	free(queue);
	queue = NULL;
	//*queue = NULL;
	return;
}

int q_push(q_t* queue, void* ptr) {
	pthread_mutex_lock(&(queue->qmutex));
	int req = 1;
	if ((queue && queue->list) && (!queue->queue_length || queue->queue_length > queue->list->count)) {
		if (ll_add_tail(queue->list, ptr))
			++req;
		else
			req = 0;
	}
	pthread_mutex_unlock(&(queue->qmutex));
	return req;
}

int q_add(q_t* queue, void* ptr) {
	pthread_mutex_lock(&(queue->qmutex));
	int req = 1;
	if ((queue && queue->list) && (!queue->queue_length || queue->queue_length > queue->list->count)) {
		if (ll_add_head(queue->list, ptr))
			++req;
		else
			req = 0;
	}
	pthread_mutex_unlock(&(queue->qmutex));
	return req;
}

int q_add_f(q_t* queue, void* ptr) {
	pthread_mutex_lock(&(queue->qmutex));
	int req = 1;
	if (queue && queue->list) {
		if (ll_add_head(queue->list, ptr))
			++req;
		else
			req = 0;
	}
	pthread_mutex_unlock(&(queue->qmutex));
	return req;
}

void* q_pop(q_t* queue) {
	pthread_mutex_lock(&(queue->qmutex));
	void* ptr = NULL;
	if ((queue && queue->list) && 0 < queue->list->count) {
		ptr = queue->list->tail->value;
		ll_del_tail(queue->list);
	}
	pthread_mutex_unlock(&(queue->qmutex));
	return ptr;
}

int q_get_length(q_t* queue) {
	pthread_mutex_lock(&(queue->qmutex));
	int len = -1;
	if (queue && queue->list)
		len = queue->list->count;
	pthread_mutex_unlock(&(queue->qmutex));
	return len;
}

//#inclide <stdio.h>

/*void q_print(q_t* queue) {
	printf("\nQlen: %d\thead_addr=[%p]\ttail_addr=[%p]\n=====\n", q_get_length(queue), queue->list->head, queue->list->tail);
	ll_node_t* li = queue->list->head;
	while (li != NULL) {
		struct ll_node_s* tmp = li;
		//free(li);
		printf("\tnode_addr=[%p],\tprev_addr=[%p],\tnext_addr=[%p],\tprt_addr=[%p]\n", tmp, tmp->prev, tmp->next, tmp->value);
		li = tmp->next;
	}
	printf("=====\n", q_get_length(queue));
}*/
