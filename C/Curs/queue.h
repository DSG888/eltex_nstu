#ifndef QUEUE_H
#define QUEUE_H

#include <pthread.h>
#include <stdlib.h>
#include "linkedlist.h"

typedef struct {
	int queue_length;
	ll_t* list;
	pthread_mutex_t qmutex;
} q_t;

q_t* q_create(int queue_length);
void q_free(q_t* queue);
int q_push(q_t* queue, void* ptr);
void* q_pop(q_t* queue);
int q_add(q_t* queue, void* ptr);
int q_add_f(q_t* queue, void* ptr);
int q_get_length(q_t* queue);
//void q_print(q_t* queue);

#endif
