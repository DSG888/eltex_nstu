#ifndef SRV_H
#define SRV_H

#include <sys/socket.h>
#include <pthread.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
//#include <alloca.h>

#include "main.h"
#include "linkedlist.h"
#include "queue.h"

#ifdef _PROTOBUF
	#include "amessage.pb-c.h"
#endif

struct srv_handler_s {
	unsigned int ip;
	int type;
	ll_t* client_list;
	q_t* msgqueue;
};

struct connect_node_s {
	unsigned int num;
	int sock;
	int type;
	pthread_t client_threadID;
	q_t* msgqueue;
} typedef connect_node_t;

int getipaddr(char* ifname, struct in_addr* ip);
int getbroadcastaddr(char* ifname, struct in_addr* bc);
void* broadcast_handler(void *args);
int create_server_socket(unsigned int ip, int port);
int accept_connection(int servSock);
void* server_worker(void *threadArgs);
void* server_handler(void *threadArgs);

#endif
