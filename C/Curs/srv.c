#include "srv.h"

// Определяет IP адрес
int getipaddr(char* ifname, struct in_addr* ip) {
	struct ifreq myip;
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	myip.ifr_addr.sa_family = AF_INET;
	snprintf(myip.ifr_name, IFNAMSIZ, ifname);
	if (ioctl(fd, SIOCGIFADDR, &myip))
		return -1;
	ip->s_addr = ((struct sockaddr_in *)&myip.ifr_addr)->sin_addr.s_addr;
	close(fd);
	return 0;
}

// Определяем BROADCAST адрес
int getbroadcastaddr(char* ifname, struct in_addr* bc) {
	struct ifreq myip;
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	myip.ifr_addr.sa_family = AF_INET;
	snprintf(myip.ifr_name, IFNAMSIZ, ifname);
	if (ioctl(fd, SIOCGIFBRDADDR, &myip))
		return -1;
	bc->s_addr = ((struct sockaddr_in *)&myip.ifr_addr)->sin_addr.s_addr;
	close(fd);
	return 0;
}

// Пока очередь не пуста, рассылает запросы на приём сообщений
// Если очередь заполнена, рассылает запросы на отправку сообщений
void* broadcast_handler(void *args) {
	int sock;
	struct sockaddr_in broadcastAddr;
	int broadcastPermission = 1;
	if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		fprintf(stderr, "socket() failed\n");
		work = 0;
		return NULL;
	}
	if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *)&broadcastPermission, sizeof(broadcastPermission)) < 0) {
		fprintf(stderr, "setsockopt() failed\n");
		work = 0;
		return NULL;
	}
	memset(&broadcastAddr, 0, sizeof(broadcastAddr));
	broadcastAddr.sin_family = AF_INET;
	broadcastAddr.sin_addr.s_addr = ((struct srv_handler_s*)args)->ip;
	broadcastAddr.sin_port = htons(SERVER_UDP_PORT);
	if (((struct srv_handler_s*)args)->type == mode_requestor)
		while (work) {
			if (q_get_length((q_t*)(((struct srv_handler_s*)args)->msgqueue)) < QSIZE) {
				if (sendto(sock, REQ_MSG, sizeof(REQ_MSG), 0, (struct sockaddr *)&broadcastAddr, sizeof(broadcastAddr)) != sizeof(REQ_MSG)) {
					fprintf(stderr, "sendto() sent a different number of bytes than expected\n");
					work = 0;
					return NULL;
				}
				if (DEBUG)
					fprintf(stderr, "Отправлен широковещательный запрос на получение сообщений\n");
			}
			sleep(K);
		}
	else
		if (((struct srv_handler_s*)args)->type == mode_distributor)
			while (work) {
				if (q_get_length((q_t*)(((struct srv_handler_s*)args)->msgqueue)) > 0) {
					if (sendto(sock, DIS_MSG, sizeof(DIS_MSG), 0, (struct sockaddr *)&broadcastAddr, sizeof(broadcastAddr)) != sizeof(DIS_MSG)) {
						fprintf(stderr, "sendto() sent a different number of bytes than expected\n");
						work = 0;
						return NULL;
					}
					if (DEBUG)
						fprintf(stderr, "Отправлен широковещательный запрос на рассылку сообщений\n");
				}
				sleep(L);
			}
		else {
			close(sock);
			DieWithError("Wrong boadcast mode");
		}
	close(sock);
	return NULL;
}

int create_server_socket(unsigned int ip, int port) {
	struct sockaddr_in server;
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (-1 == sock) {
		fprintf(stderr, "Could not create socket\n");
		exit(EXIT_FAILURE);
	}
	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = ip;
	server.sin_port = htons(port);
	if (bind(sock, (struct sockaddr*)&server , sizeof(server)) < 0) {
		fprintf(stderr, "bind failed\n");
		exit(EXIT_FAILURE);
	}
	if (listen(sock, MAXPENDING) < 0) {
		fprintf(stderr, "listen failed\n");
		exit(EXIT_FAILURE);
	}
	return sock;
}

int accept_connection(int servSock) {
	int clntSock;
	struct sockaddr_in echoClntAddr;
	unsigned int clntLen = sizeof(echoClntAddr);
	if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)
		DieWithError("accept() failed");
	fprintf(stdout, "Подключен клиент с ip %s%s%s\n", cl_blue, inet_ntoa(echoClntAddr.sin_addr), cl_normal);
	return clntSock;
}

void* server_worker(void *threadArgs) {
	/* Guarantees that thread resources are deallocated upon return */
	pthread_detach(pthread_self()); 
	
	int sock = ((struct connect_node_s*)threadArgs)->sock;
	
	if (mode_client1 == ((struct connect_node_s*)threadArgs)->type) {
		while (work) {
			char* tmp = NULL;
			if (!(tmp = recv_msg(sock, 1))) {
				close(sock);
				break;
			}
			fprintf(stdout, " Получил %s%s%s от клиента №%s%d%s.\n", cl_blue, tmp, cl_normal, cl_red, ((struct connect_node_s*)threadArgs)->num, cl_normal);
			char buf[40] = {'\0'};
			snprintf(buf, 40, "cl_%d: ", ((struct connect_node_s*)threadArgs)->num);
			int slen = strlen(tmp);
			char* str = malloc(sizeof(char) * (strlen(buf) + slen + 1));
			if (!str)
				DieWithError("mem error");
		//	strcpy(str, buf);
		//	strcpy(str + strlen(buf), tmp);
		//	str[strlen(buf) + slen] = '\0';
		
			snprintf(str, slen + strlen(tmp) + 1, "cl_%d: %s", ((struct connect_node_s*)threadArgs)->num, tmp);
		
			int req = 1;
			if (q_push((q_t*)((struct connect_node_s*)threadArgs)->msgqueue, str)) {
				fprintf(stdout, "[%s-%s]\n", cl_red, cl_normal);
				free(str);
			}
			else {
				fprintf(stdout, "[%s+%s]\n", cl_green, cl_normal);
				req = 0;
			}
			if (send(sock, &req, sizeof(req), 0) != sizeof(req)) {
				close(sock);
				break;
			}
		}
	}
	else
		if (mode_client2 == ((struct connect_node_s*)threadArgs)->type) {
			while (work) {
				if (q_get_length(((struct connect_node_s*)threadArgs)->msgqueue) < 1) {
					sleep(1);
					continue;
				}
				char* str = (char*)q_pop((q_t*)((struct connect_node_s*)threadArgs)->msgqueue);
				if (!str) {
					sleep(1);//FIXME
					continue;
				}
				int slen = strlen(str);
				if (send_msg(sock, str, slen)) {
		//			q_add_f((q_t*)((struct connect_node_s*)threadArgs)->msgqueue, str);	// FIXME? Без проверки?
					q_push((q_t*)((struct connect_node_s*)threadArgs)->msgqueue, str);//FIXME
					printf("%s Вернул назад!%s\n", cl_red, cl_normal);
					free(str);
					sleep(1);
					close(sock);
					break;
					//continue;
				}
				fprintf(stdout, " Отправил %s%s%s клиенту №%s%d%s.\n", cl_blue, str, cl_normal, cl_red, ((struct connect_node_s*)threadArgs)->num, cl_normal);
				free(str);
				//sleep(1);
			}
		}
		else {
			DieWithError("wrong client type..");		
		}
	//close(sock);
	return NULL;
} 

void* server_handler(void *threadArgs) {
	pthread_detach(pthread_self());		// Guarantees that thread resources are deallocated upon return

	int port = 0;
	if (mode_client1 == ((struct srv_handler_s*)threadArgs)->type)
		port = CLIENT1_TCP_PORT;
	else
		if (mode_client2 == ((struct srv_handler_s*)threadArgs)->type)
			port = CLIENT2_TCP_PORT;
		else
			DieWithError("wrong client type");
	int client_count = 0;
	int sock = create_server_socket(((struct srv_handler_s*)threadArgs)->ip, port);
	connect_node_t* newconnect = NULL;
	while (work) {
		newconnect = malloc(sizeof(connect_node_t));
		if (!newconnect)
			DieWithError("memerror");
		newconnect->num = client_count++;
		newconnect->type = ((struct srv_handler_s*)threadArgs)->type;
		newconnect->sock = accept_connection(sock);
		newconnect->msgqueue = (q_t*)((struct srv_handler_s*)threadArgs)->msgqueue;
		if (pthread_create(&newconnect->client_threadID, NULL, server_worker, (void*)newconnect) != 0) {
			DieWithError("\npthread_create() failed");
		}
		//fprintf(stdout, " в потоке %ld\n", (long int)newconnect->client_threadID);
		ll_add_tail((ll_t*)((struct srv_handler_s*)threadArgs)->client_list, newconnect);
	}
	return NULL;
}
