/*
	2. Написать сервер и 2 типа клиента. На сервере работает 
синхронизированная очередь из N сообщений. Идет прием сообщений и 
отправка клиентам одновременно по TCP протоколу.
	Если очередь не заполнена, то сервер каждые K секунд посылает UDP 
пакет "Жду сообщений" в локальную сеть. Если клиент 1-го типа получает 
этот пакет, то отправляет по TCP сообщение (время T обработки 
сообщения, длина случайной строки, случайная строка) . После отправки 
сообщения клиент засыпает на время T. Строка случайной длины и время 
T-генерируется случайно, максимальная длина строки и интервал времени 
задается через константы. Как только очередь заполняется, то сервер 
перестает слать UDP оповещение.
	Если в очереди есть сообщения, то сервер посылает каждые L секунд 
UDP пакет "есть сообщения" в локальную сеть. Клиент 2-го типа получив 
такой UDP пакет устанавливает соединение с сервером и получает по TCP 
от сервера сообщение со строкой. Обрабатывает T секунд(просто спит) и 
только после сможет опять получать сообщения. Как только очередь 
опусташается, то сервер перестает слать UDP оповещение.
	Кол-во клиентов 1-го и 2-го типа может быть неограниченно.
	Сделать make-файл для сборки сервера и клиентов. Реализовать сервер 
и клиенты на языке С. Использовать компилятор gcc. Стандарт языка С99. 
Результаты работы разместить на сайте git-репозитория (bitbucket.org, 
github.com и т.д.). Пригласить преподавателя на сайте git-репозитория в 
качестве редактора ресурсов для проверки результатов.
	Допускается написание сервера на Java, а клиентов на С.
*/

#include "main.h"

/* TODO List
[+] Segmentation fault сервера при странных обстоятельствах
[ ] Убрать лишние включения
[ ] Утечки памяти в потоках сервера
[ ] Обработка события KeyboardInterrupt
[ ] Проверить q_add_f и FIXME в server_worker
[ ] Добавить комментарии
[ ] Заменить все unsigned int в uint_t и дз. иш stdint.h
[ ] DEBUG задавать константой в Makefile
[-] Разбить на 3 исполняемых файла?
*/

const int DEBUG = 1;
int work = 1;

int send_msg(int so, char* msg, int slength) {
	void *buf = msg;
	int len = sizeof(char) * slength;
	#ifdef _PROTOBUF
	AMsg smsg = AMSG__INIT;
	smsg.str = msg;
	len = amsg__get_packed_size(&smsg);
	buf = alloca(len);
	amsg__pack(&smsg, buf);
	#endif
	int req = 1;
	if ((send(so, &len, sizeof(len), 0) != sizeof(len)))
		return 1;
	if (send(so, buf, len, 0) != len)
		return 2;
	if (recv(so, &req, sizeof(req), 0) != sizeof(req))
		return 3;
	if (req)
		return 4;
	return 0;
	
}

char* recv_msg(int so, int req) {
	int slen = 0;
	char* str = NULL;
	void* buf = NULL;
	if ((recv(so, &slen, sizeof(slen), 0)) != sizeof(slen))
		return NULL;
	buf = alloca(slen);
	if (recv(so, buf, slen, 0) != slen)
		return NULL;
	#ifdef _PROTOBUF
	AMsg* smsg = amsg__unpack(NULL, slen, buf);
	if (smsg) {
		buf = smsg->str;
		slen = strlen(buf);
	}
	#endif
	if (!(str = malloc(sizeof(char) * (slen + 1))))
		return NULL;
	strcpy(str, buf);
	#ifdef _PROTOBUF
	if (smsg)
		amsg__free_unpacked(smsg, NULL);
	#endif
	if (!req && (send(so, &req, sizeof(req), 0) != sizeof(req))) {
		free(str);
		return NULL;
	}
	str[slen] = '\0';
	return str;
}

void DieWithError(char *errorMessage) {
	if (errorMessage)
		fprintf(stderr, "%s\n", errorMessage);
	getchar();
	exit(EXIT_FAILURE);
}

int main(int argc, char * argv[]) {
	if (3 > argc)
		DieWithError("args error");
	struct in_addr myipaddr = {0};
	struct in_addr broadcastaddr = {0};
	if (getipaddr(argv[2], &myipaddr)) {
		fprintf(stderr, "Wrong interface \"%s\"\n", argv[2]);
		return EXIT_FAILURE;
	}
	srand(time(0)^getpid());

	switch(atoi(argv[1])) {
		case mode_server:
			if (getbroadcastaddr(argv[2], &broadcastaddr))
				DieWithError("Wrong interface");
			fprintf(stdout, "Режим сервера\n");
			pthread_t thread_msg_bcsender_tr;
			pthread_t thread_msg_bcsender_rc;
			pthread_t thread_server_handler_1;
			pthread_t thread_server_handler_2;
			q_t* msgqueue = q_create(QSIZE);
			struct srv_handler_s arg1 = {myipaddr.s_addr, mode_client1, ll_create(), msgqueue};
			struct srv_handler_s arg2 = {myipaddr.s_addr, mode_client2, ll_create(), msgqueue};
			struct srv_handler_s arg3 = {broadcastaddr.s_addr, mode_requestor, NULL, msgqueue};
			struct srv_handler_s arg4 = {broadcastaddr.s_addr, mode_distributor, NULL, msgqueue};			
			if ((pthread_create(&thread_msg_bcsender_tr, NULL, broadcast_handler, &arg3) < 0) ||
					(pthread_create(&thread_msg_bcsender_rc, NULL, broadcast_handler, &arg4) < 0) ||
					(pthread_create(&thread_server_handler_1, NULL, server_handler, &arg1) < 0) ||
					(pthread_create(&thread_server_handler_2, NULL, server_handler, &arg2) < 0))
				DieWithError("could not create thread");
			while (work) {
				sleep(1);
			}
			break;
		case mode_client1:
			signal(SIGPIPE, SIG_IGN);
			fprintf(stdout, "Режим клиента 1го типа\n");
			pthread_t thread_msg_sender;
			if (pthread_create(&thread_msg_sender, NULL, client1_handler, &myipaddr.s_addr) < 0)
				DieWithError("could not create thread");
			while (work) {
				sleep(1);
			}
		break;
		case mode_client2:
			fprintf(stdout, "Режим клиента 2го типа\n");
			pthread_t thread_msg_receiver;
			if (pthread_create(&thread_msg_receiver, NULL, client2_handler, &myipaddr.s_addr) < 0)
				DieWithError("could not create thread");
			while (work) {
				sleep(1);
			}
			break;
		default:
			DieWithError("args error");
			break;
	}
	return EXIT_SUCCESS;
}
