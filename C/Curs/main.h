#ifndef MAIN_H
#define MAIN_H

#define QSIZE 10						// Размер очереди
#define K 1								// Таймер отправки запросов на сообщения
#define L 1								// Таймер отправки запросов на сообщения
#define SERVER_UDP_PORT 3340			// Этот порт слушают клиенты
#define CLIENT1_TCP_PORT 3341			// Через этот порт сервер получает сообщения
#define CLIENT2_TCP_PORT 3342			// Через этот порт сервер отправляет сообщения
#define REQ_MSG "WAITING FOR MESSAGES"	// Сообщение для клиентов 1го типа
#define DIS_MSG "HAVE MESSAGES"			// Сообщение для клиентов 2го типа

#define MAXPENDING 32					// Максимум клиентов

#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <pthread.h>
#include <signal.h>

#include "main.h"
#include "srv.h"
#include "cli.h"
#include "queue.h"

#define cl_green "\033[32m"
#define cl_blue "\033[34m"
#define cl_red "\033[31m"
#define cl_normal "\033[0m" 

#ifdef _PROTOBUF
	#include "amessage.pb-c.h"
#endif

enum mode {mode_server = 0, mode_client1 = 1, mode_client2 = 2};
enum bc_mode {mode_requestor = 1, mode_distributor = 2};

int work;
extern const int DEBUG;

void DieWithError(char *errorMessage);
int send_msg(int so, char* msg, int slength);
char* recv_msg(int so, int req);

#endif
