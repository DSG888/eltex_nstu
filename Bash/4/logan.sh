#!/bin/bash

# $ bash logan.sh apt 2018 05 20

filearray=$( find /var/log -name "*log*" -print 2>/dev/null |grep $1 )
filearrayn=(  )

#CONSTs
#{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}
dataarray=( "$2@$3@$4" "$2@$4@$3" "$3@$2@$4" "$3@$4@$2" "$4@$2@$3" "$4@$3@$2" )
sep=( ":" "-" "/" "." )
SETCOLOR_SUCCESS="echo -en \\033[1;32m"
SETCOLOR_FAILURE="echo -en \\033[1;31m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"

echo "Анализ:"
for p in ${filearray[@]}; do
	EXTENSION="${p##*.}"
	case $EXTENSION in
		"log")
			textfile="$(cat $p 2>/dev/null)"
			printf "Текстовый: "$p
			;;
		"gz")
			textfile="$(gzip -qd $p -c 2>/dev/null)"
			printf "Из архива: "$p
			;;
		*)
			$SETCOLOR_FAILURE
			printf "Пропущен:"
			$SETCOLOR_NORMAL
			echo " $p"
			continue
			;;			
	esac
	if [ "$?" == "0" -a "$textfile" != "" ]; then
		$SETCOLOR_SUCCESS
		printf "$(tput hpa $(tput cols))$(tput cub 6)[OK] "
		$SETCOLOR_NORMAL
	else
		$SETCOLOR_FAILURE
		echo "$(tput hpa $(tput cols))$(tput cub 6)[FAIL]"
		$SETCOLOR_NORMAL
		continue
	fi

	flag=0
	for s in ${sep[@]}; do
		if [ $flag -eq 1 ]; then
			break
		fi
		for d in ${dataarray[@]}; do
			str="$(echo "$d" |  awk "{ gsub(\"@\",\"$s\"); print }")"
			if [ "$(echo $textfile | grep $str)" != "" ]; then
				flag=1
				filearrayn=( $filearrayn $p )
				echo "+"
				break
			fi
		done
	done
	if [ "$flag" -eq 0 ]; then
		echo "-"
	fi
done
echo ""

filearrayn=( $filearrayn Quit )
PS3="?> "
select opt in "${filearrayn[@]}"
do
	if [ $opt == "Quit" ]; then
		exit
	else
		EXTENSION="${opt##*.}"
		case $EXTENSION in
			"log")
				textfile="$(cat $p 2>/dev/null)"
				cat "$textfile"
				;;
			"gz")
				textfile="$(gzip -qd $p -c 2>/dev/null)"
				cat "$textfile"
				;;
			*)
				$SETCOLOR_FAILURE
				printf "Пропущен:"
				$SETCOLOR_NORMAL
				echo " $p"
				;;			
		esac
		echo ""
		echo "$REPLY"
	fi
done
