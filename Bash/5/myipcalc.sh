#!/bin/sh

ClBlue="\e[34m"
ClNormal="\e[0m"

pow32=$((1 << 32))
pow24=$((1 << 24))
pow16=$((1 << 16))
pow8=$((1 << 8))

is_number()
{
	if [ $# -ne 1 -o -z "$1" ]; then
		return 1
	fi
	if [ "$1" -eq "$1" ] 2> /dev/null; then
		return 0
	fi
	return 1
}

todec()
{
	local __ipv4 __oct10 _oct1 _oct2 _oct3 _oct4 _oct
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	__ipv4="$1"
	__oct10="$2"
	_oct1=${__ipv4%.*.*.*}
	_oct2=${__ipv4%.*.*};		_oct2=${_oct2#*.}
	_oct3=${__ipv4#*.*.};		_oct3=${_oct3%.*}
	_oct4=${__ipv4#*.*.*.}
	for _oct in "${_oct1}" "${_oct2}" "${_oct3}" "${_oct4}"; do
		if [ -z "${_oct}" -o "${_oct}" = "${__ipv4}" ]; then
			return 1
		fi

		if ! is_number "${_oct}"; then
			return 1
		fi

		if [ "${_oct}" -lt 0 -o "${_oct}" -gt "$(($pow8 - 1))" ]; then
			return 1
		fi
	done
	eval eval \${__oct10}=$((${_oct1} << 24 | ${_oct2} << 16 | ${_oct3} << 8 | ${_oct4}))
	return 0
}

conv_netmask()
{
	local __ipv4 __oct10 _oct t zmask

	__ipv4="$1"
	__oct10="$2"

	for i in {32..1}; do
		toip "$((((1<<${i}) - 1)^(pow32 - 1)))" "zmask"
		if [ $(echo "$1" | grep "$zmask" | wc -l) == 1 ]; then		# if [ "$1" == "$zmsak" ]; then
			t=$((32-$i))
			break
		fi
	done
	eval eval \${__oct10}=${t}
	return 0
}

toip()
{
	local __oct10 __ipv4 _oct1 _oct2 _oct3 _oct4
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	__oct10="$1"
	__ipv4="$2"
	if ! is_number "${__oct10}"; then
		return 1
	fi
	if [ "${__oct10}" -lt 0 -o "${__oct10}" -gt "$(($pow32 - 1))" ]; then
		return 1
	fi
	_oct1=$((((${__oct10} >> 24)) & 0xFF))
	_oct2=$((((${__oct10} >> 16)) & 0xFF))
	_oct3=$((((${__oct10} >> 8)) & 0xFF))
	_oct4=$((${__oct10} & 0xFF))
	eval eval \${__ipv4}="${_oct1}.${_oct2}.${_oct3}.${_oct4}"
	return 0
}

is_ipv4_belong_net()
{
	local _ipv4 _network _mask _ip_net
	local _oct10 _mask10 _ip_net10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_ipv4="$1"
	_net="$2"
	_network="${_net%/*}"
	_mask="${_net#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	if ! todec "${_ipv4}" "_oct10"; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$(($pow32 - ${_hosts}))
	_ip_net10=$((${_oct10} & ${_mask10}))
	if ! toip "${_ip_net10}" "_ip_net"; then
		return 1
	fi
	if [ "${_ip_net}" != "${_network}" ]; then
		return 1
	fi
	return 0
}

get_netmask()
{
	local _net _netmask _network _mask _hosts _mask_tmp
	local _mask10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_net="$1"
	_netmask="$2"
	_network="${_net%/*}"
	_mask="${_net#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$(($pow32 - ${_hosts}))
	if ! toip "${_mask10}" "_mask_tmp"; then
		return 1
	fi
	eval eval \${_netmask}="${_mask_tmp}"
	return 0
}

get_wildcard()
{
	local _net _netmask _network _mask _hosts _mask_tmp
	local _mask10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_net="$1"
	_netmask="$2"
	_network="${_net%/*}"
	_mask="${_net#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$((($pow32 - ${_hosts}) ^ (pow32-1)))
	if ! toip "${_mask10}" "_mask_tmp"; then
		return 1
	fi
	eval eval \${_netmask}="${_mask_tmp}"
	return 0
}

get_network()
{
	local _ipv4mask _network _ipv4 _mask _hosts _network_tmp
	local _oct10 _mask10 _network10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_ipv4mask="$1"
	_network="$2"
	_ipv4="${_ipv4mask%/*}"
	_mask="${_ipv4mask#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	if ! todec "${_ipv4}" "_oct10"; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$(($pow32 - ${_hosts}))
	_network10=$((${_oct10} & ${_mask10}))
	if ! toip "${_network10}" "_network_tmp"; then
		return 1
	fi
	eval eval \${_network}="${_network_tmp}/${_mask}"
	return 0
}

get_minhost()
{
	local _net _minhost _network _mask _hosts _minhost_tmp
	local _network10 _mask10 _minhost10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_net="$1"
	_minhost="$2"
	_network="${_net%/*}"
	_mask="${_net#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	if [ "${_mask}" -eq 31 ]; then
		eval eval \${_minhost}="${_network}"
		return 0
	fi
	if [ "${_mask}" -eq 32 ]; then
		return 0
	fi

	if ! todec "${_network}" "_network10"; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$(($pow32 - ${_hosts}))
	_minhost10=$((${_network10} + 1))
	if ! toip "${_minhost10}" "_minhost_tmp"; then
		return 1
	fi
	eval eval \${_minhost}="${_minhost_tmp}"
	return 0
}

get_maxhost()
{
	local _reserve _net _maxhost _network _mask _hosts _maxhost_tmp
	local _network10 _mask10 _maxhost10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_reserve=2
	_net="$1"
	_maxhost="$2"
	_network="${_net%/*}"
	_mask="${_net#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	if [ "${_mask}" -ge 31 ]; then
		_reserve=1
	fi
	if ! todec "${_network}" "_network10"; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$(($pow32 - ${_hosts}))
	_maxhost10=$((${_network10} + ${_hosts} - ${_reserve}))
	if ! toip "${_maxhost10}" "_maxhost_tmp"; then
		return 1
	fi
	eval eval \${_maxhost}="${_maxhost_tmp}"
	return 0
}

get_broadcast()
{
	local _net _broadcast _network _mask _hosts _broadcast_tmp
	local _network10 _mask10 _broadcast10
	if [ $# -ne 2 -o -z "$1" -o -z "$2" ]; then
		return 1
	fi
	_net="$1"
	_broadcast="$2"
	_network="${_net%/*}"
	_mask="${_net#*/}"
	if ! is_number "${_mask}"; then
		return 1
	fi
	if [ "${_mask}" -lt 0 -o "${_mask}" -gt 32 ]; then
		return 1
	fi
	if [ "${_mask}" -ge 31 ]; then
		return 0
	fi
	if ! todec "${_network}" "_network10"; then
		return 1
	fi
	_hosts=$((1 << $((32 - ${_mask}))))
	_mask10=$(($pow32 - ${_hosts}))
	_broadcast10=$((${_network10} + ${_hosts} - 1))
	if ! toip "${_broadcast10}" "_broadcast_tmp"; then
		return 1
	fi
	eval eval \${_broadcast}="${_broadcast_tmp}"
	return 0
}

ipcalc()
{
	local ip num_mask netmask network minhost maxhost broadcast hosts reserve wildcard
	if [ $# -ne 1 ]; then
		echo "Запуск: $0 <отктет1>.<отктет2>.<отктет3>.<отктет4>/маска"
		exit 1
	fi
	ip=${1%/*};
	num_mask=${1#*/}
	
	if ! is_number "$num_mask" ; then
		if ! conv_netmask "$num_mask" "num_mask"; then
			echo "Wrong netmask"
			exit 1
		fi
	fi
	
	if [ "$ip" = "$1" -o "$num_mask" = "$1" ]; then
		echo "Запуск: $0 <отктет1>.<отктет2>.<отктет3>.<отктет4>/маска"
		exit 1
	fi
	if ! get_netmask "$ip/$num_mask" "netmask"; then
		echo "Wrong netmask"
		exit 1
	fi
	if ! get_wildcard "$ip/$num_mask" "wildcard"; then
		echo "Wrong netmask"
		exit 1
	fi
	if ! get_network "$ip/$num_mask" "network"; then
		echo "Wrong IP"
		exit 1
	fi
	get_minhost "$network" "minhost"
	get_maxhost "$network" "maxhost"
	get_broadcast "$network" "broadcast"
	hosts=$((1 << $((32 - $num_mask))))
	reserve=2
	if [ "$num_mask" -ge 31 ]; then
		reserve=0
	fi
	echo -e "Address:\t$ClBlue$ip$ClNormal"
	echo -e "Netmask:\t$ClBlue$netmask = $num_mask$ClNormal"
	echo -e "Wildcard:\t$ClBlue$wildcard$ClNormal"
	echo "=>"
	echo -e "Network:\t$ClBlue$network$ClNormal"
	echo -e "HostMin:\t$ClBlue$minhost$ClNormal"
	echo -e "HostMax:\t$ClBlue$maxhost$ClNormal"
	echo -e "Broadcast:\t$ClBlue$broadcast$ClNormal"
	echo -e "Hosts:\t\t$ClBlue$(($hosts-$reserve))$ClNormal"
	echo ""
}

if [ $# -ge 1 ]; then
	ipcalc "$@"
fi
