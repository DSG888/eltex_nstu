#!/bin/bash

#bash script <dir> <num1> <template>

#1. Скриптом на BASH, создать 5 папок в указанной, в каждой из 5ти папок создать еще 10 
#подпапок, и в каждой из этих подпапок создать 20 пустых файлов. Модернизировать 
#скрипт, добавив в него возможность пользователю выбирать место расположения папок, 
#их количество, шаблон имени и т.д.

#mkdir {1..5}

SETCOLOR_SUCCESS="echo -en \\033[1;32m"
SETCOLOR_FAILURE="echo -en \\033[1;31m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"

#$1 (dir)
if ! [ -d $1 ]; then
	echo "No directory"
	exit
fi

#$2 (num)
if (echo "$2" | grep -E -q "^?[0-9]+$" > /dev/null); then
	$SETCOLOR_SUCCESS
	echo -n "param2... $(tput hpa $(tput cols))$(tput cub 6)[OK]"
	$SETCOLOR_NORMAL
	echo
else
	$SETCOLOR_FAILURE
	echo -n "param2... $(tput hpa $(tput cols))$(tput cub 6)[fail]"
	$SETCOLOR_NORMAL
	echo
	echo "#bash script <dir> <num> <template>"
	exit
fi

#mkdir $1/{1..$2}
for (( i=1; i <= $2; i++ ))
do
	mkdir $1/$i
	for (( j=1; j <= 20; j++ ))
	do
		mkdir $1/$i/$j
		for (( k=1; k <= 10; k++ ))
		do
			touch $1/$i/$j/$3_$k
		done
	done
done
