#!/bin/bash

echo "Первое число:"
read A
echo "Второе число:"
read B
echo ""
echo "Действие:"

Q="%"
while [ $Q != "@" ]
do
	read Q
	case $Q in
		"+")
			echo "$A + $B =" $(expr $A + $B)
			;;
		"-")
			echo "$A - $B =" $(expr $A - $B)
			;;
		"/")
			if [ $B -eq 0 ]
			then
				echo "Ошибка: Деление на 0";
			else
				echo " $A / $B =" $(expr $A / $B)"."$(expr $A % $B);
			fi
			;;
		"*")
			echo " $A * $B =" $(expr  $A \* $B)
			;;
		*)
			echo "Действия: '+' '-' '*' '/'"
			;;
	esac
done
