#!/bin/bash

if [ "$1" == "" ]; then
	mypath="$(zenity --file-selection)"
else
	mypath=$1
fi

if [ "$mypath" != "" ]; then
	mytext=$(cat "$mypath")
	mytext=$(echo -n "$mytext" | zenity --title="Текстовый редактор" --ok-label="Сохранить" --cancel-label="Выйти" --text-info --editable --width=600 --height=800)
	case $? in
		0)
			echo -n "$mytext" > "$mypath"
			;;
		*)
			echo "Пользователь просто вышел"
			;;
	esac
fi
