#!/bin/bash

#procmanager.sh xclock

CRON=$(pstree -s $$ | grep -q cron && echo 1 || echo 0)

print_menu()
{
	echo "Действия:"
	echo " [1] Запустить ещё"
	echo " [2] Статус"
	echo " [3] Снять процесс с выполнения"
	echo " [4] Убрать мертвые процессы из списка"
	echo " [0] Выход"
}

check_status_p()
{
	i=0
	echo "Список процессов $1:"
	for pid in $@; do
		if [ $i -gt 0 ]; then
			#STATUS=$(pstree -s $pid | grep -q $1 && echo жив || echo мертв)
			STATUS=$(if [ $(ps -l $pid | grep $1 | wc -l) -eq 1 ]; then echo "жив"; else echo "мертв"; fi)
			echo " [$i] $pid $STATUS"
		fi
		((i++))
	done
}

DATE="["`date '+%Y-%m-%d@%H:%M:%S'`"]"
read -a PIDList <<< $(pidof $1)

#Проверка Cron-а
if [ "$CRON" -eq "1" ]; then
	DISPLAY=":0.0"
	export $DISPLAY
	echo "$DATE Скипт вызвался под кроном" >> /tmp/log
else
	echo "$DATE Скипт вызвался НЕ под кроном" >> /tmp/log
fi

#Проверка процесса
if [ ${#PIDList[*]} -gt 0 ]; then #len($PIDList) > 0
	echo -e "$DATE $1 уже запущен "${#PIDList[*]}" раз.\tPID: "${PIDList[@]}
else
	$1 &
	#PIDList=$$
	read -a PIDList <<< $!
	echo -e "$DATE $1 не запущен. Запускаю... PID: "${PIDList[@]}
fi

#Меню
if [ "$CRON" -eq "0" ]; then
	Q=-1
	while [ $Q -ne 0 ]
	do		
		Q=" "
		while !(echo "$Q" | grep -E -q "^?[0-9]+$"); do
			echo ""
			print_menu
			printf "?> "
			read Q
		done
		case $Q in
		0)
			exit
		;;
		1)
			$1 &
			PIDList=( "${PIDList[@]}" "$!" )
		;;
		2)
			check_status_p $1 ${PIDList[@]}
			echo "----------"
		;;
		3)
			if [ "$(pidof $1)" ]; then
				QQ=-1
				while [ $QQ -ne 0 ]; do
					QQ=" "
					while !(echo "$QQ" | grep -E -q "^?[0-9]+$"); do
						check_status_p $1 ${PIDList[@]}
						echo " [0] Я передумал"
						printf "ID> "
						read QQ
					done
					if [ "$QQ" -gt 0  -a "$QQ" -le ${#PIDList[*]} ]; then
						if [ $(ps -l "${PIDList[$QQ - 1]}" | grep $1 | wc -l) -eq 1 ]; then
							kill -9 "${PIDList[$QQ - 1]}"
						else
							echo "Процесс уже мертв"
						fi
						break
					else
						if [ ! "$QQ" -eq "0" ]; then
							echo "Нет такого ID"
						fi
					fi;
				done
			else
				echo "Все экземпляры уже закрыты"
			fi
		;;
		4)
			read -a PIDList <<< $(pidof $1)
			check_status_p $1 ${PIDList[@]}
			echo "----------"
			;;
		*)
			echo "Ошибка"
		;;
		esac
	done
fi
