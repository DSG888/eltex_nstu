#!/bin/bash

#$1 (param)
if [ -b $1 ]; then
	#BLOCK
	dd if=$1 of="/tmp/bkps/backup-["`date '+%Y-%m-%d@%H-%M-%S'`"].img"
	exit
fi

if [ -d $1 ]; then
	#Arc
	tar -cvjpf "/tmp/bkps/backup-["`date '+%Y-%m-%d@%H-%M-%S'`"].tar.bz2" $1/*

	#RSYNC
	dir="backup-["`date '+%Y-%m-%d@%H-%M-%S'`"]"
	mkdir /tmp/bkps/$dir
	rsync -av $1 /tmp/bkps/$dir
	#cp -r $1 /tmp/bkps/$dir
	exit
fi

if [ -f $1 ]; then
	#RSYNC
	dir="backup-["`date '+%Y-%m-%d@%H-%M-%S'`"]"
	mkdir /tmp/bkps/$dir
	rsync -av $1 /tmp/bkps/$dir
	#cp -r $1 /tmp/bkps/$dir
	exit
fi

echo "ERROR. Bad param"
