#!/bin/bash

Q=-1
hour=-1
min=-1

print_menu()
{
	echo ""
	echo "[1] Новый будильник"
	echo "[2] Удалить будильник"
	echo "[3] Список будильников"
	echo "[4] Сохранить и выйти"
	echo "[0] Выход"
}

print_alarms()
{
	i=1
	echo "Список будильников:"
	while read line_x; do
		read min hou tx <<< "$line_x";
		echo "$i) $hou : $min";
		((i++));
	done < /tmp/alist

	if [ $i -eq 1 ]; then
		echo "  Список пуст";
	fi;
	echo "__________________";
	echo "";
}

update_cron()
{
	crontab -l > /tmp/temp | grep "#ALARM"
	cat /tmp/alist >> /tmp/temp
	crontab /tmp/temp
}

create_alarm()
{
	echo "Создание нового будильника"
	printf "Часы ?>"
	read hour
	printf "Минуты ?>"
	read min
	str="$min $hour * * * /usr/bin/mplayer /tmp/song.mp3 #ALARM"
	echo "$str" >> /tmp/alist
	echo "Добавлен"
}

delalarm()
{
	echo "Удаление"
	printf "ID > "
	read i
	sed -i "$i d" /tmp/alist
	echo "Удалён"
}

print_menu
while [ $Q -ne 0 ]
do
  printf "?> "
  read Q
  case $Q in
	0)
		echo "Выход"
	;;
	1)
		create_alarm
	;;
	2)
		delalarm
	;;
	3)
		print_alarms
	;;
	4)
		update_cron
		exit
	;;

	*)
		echo "Ошибка"
	;;
  esac
done
